<?php

/*
Página responsável por:
Mostrar formulário de Criação/Edição de área;
Cadastrar área caso não tenha sido passado ID;
Editar área se o ID foi passado.
*/

use DAO\Area;


// Caso tenha recebido id por POST deve cadastrar ou editar
// e reedirecionar para listagem
if(isset($_POST['descricao'])) {
  // Cadastrar Área que não recebeu id
  if ($_POST['id'] == null) {
    $resposta=Area::getInstance()->insertUnique($_POST['descricao']);
  }
  // Edita área
  else {
    $resposta=Area::getInstance()->updateUnique($_POST['id'], $_POST['descricao']);
  }
  require_once ('views/areas.php');
  die();
}
// Caso tenha recebido id por GET deve mostrar campos para edição de categoria existente
else if(!empty($_GET['id'])) {
  $area = Area::getInstance()->filtrarPorId($_GET['id']);
  $id = $area->id;
  $descricao = $area->descricao;
}
// Caso não tenha recebido id deve mostrar campos para criação de categoria
else {
  $id = "";
  $descricao = "";
}





if(!empty($_GET['id'])) {
  $area = Area::getInstance()->filtrarPorId($_GET['id']);
  $id = $area->id;
  $descricao = $area->descricao;
}

if(isset($_POST["nome"]) && isset($_POST["email"]) && isset($_POST["cidade"]) && isset($_POST["uf"]))
{
  if(empty($_POST["nome"]))
  $erro = "Campo nome obrigatório";
  else
  if(empty($_POST["email"]))
  $erro = "Campo e-mail obrigatório";
  else
  {
    //Vamos realizar o cadastro ou alteração dos dados enviados.
  }
}
?>
<div class="container">
  <form action="/?path=editar-area" method="POST">
    <input type="hidden" name="id" value="<?=$id?>">
    <div class="form-group">
      <label for="descricao">Nome</label>
      <input type="text" id="descricao" name="descricao" class="form-control" placeholder="Nome da área" value="<?=$descricao?>">
    </div>
    <button type="submit" class="btn btn-primary">Enviar</button>
  </form>
</div>
