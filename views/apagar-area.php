<?php

/*
Página responsável por:
Mostrar confirmação da exclusão de área;
Excluir área.
*/

use DAO\Area;

// Exclusão após receber id por post
if(!empty($_POST['id'])) {
  $resposta = Area::getInstance()->deleteWSafety($_POST['id']);
  require_once ('views/areas.php');
  die();
}
// Antes da exclusão confirmar nome da área
else if (!empty($_GET['id'])) {
  $area = Area::getInstance()->filtrarPorId($_GET['id']);
  $id = $area->id;
  $descricao = $area->descricao;
} else {
  require_once ('views/areas.php');
  die();
}
?>

<?php if(isset($_GET['id'])) : ?>

  <div class="container">
    <form action="/?path=apagar-area" method="POST">

      <div class="alert alert-danger" role="alert">
        <p>A área <?=$descricao?> será apagada, tem certeza disso?</p>
      </div>
      <input type="hidden" name="id" value="<?=$id?>">
      <button type="submit" class="btn btn-primary">Sim</button>
    </form>
  </div>

    <?php endif; ?>
