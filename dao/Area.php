<?php

namespace DAO;

class Area extends Database {

  const TABLE = 'area';
  protected static $oInstance;

  public function insertUnique($value)
  {
    $dbst = $this->db->prepare("SELECT descricao FROM " . static::TABLE. " WHERE descricao = :descricao");
    $dbst->bindParam(':descricao', $value);
    $dbst->execute();
    $nRepeticoes = $dbst->rowCount();

    if ($nRepeticoes === 0) {
      return $this->insert(['descricao' => $value]);
    }
    return false;
  }
  public function updateUnique($id, $value)
  {
    $dbst = $this->db->prepare("SELECT descricao FROM " . static::TABLE. " WHERE descricao = :descricao");
    $dbst->bindParam(':descricao', $value);
    $dbst->execute();
    $nRepeticoes = $dbst->rowCount();

    if ($nRepeticoes === 0) {
      return $this->update($id, ['descricao' => $value]);
    }
    return false;
  }
  public function deleteWSafety($id)
  {
    $dbst = $this->db->prepare("SELECT area FROM melhorias WHERE area = :area");
    $dbst->bindParam(':area', $id);
    $dbst->execute();
    $nRepeticoes = $dbst->rowCount();

    if ($nRepeticoes === 0) {
      return $this->delete($id);
    }
    return false;
  }
}
